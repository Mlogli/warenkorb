<?php


require_once "./models/Book.php";

$count = 0;

$bookList = Book::getAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" ;

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">


    <style>
        .form .form-group {
            text-align: left;
        }
    </style>
    <title>Bücher</title>
</head>
<body>
<div class="container">

    <div class="row">
        <div class="col-sm-10 form-group"><h1>Bücher</h1></div>
        <br>
        <div class="col-sm-2">
            <input type="submit"
                   name="cart"
                   class="btn btn-primary btn-block"
                   value="Warenkorb: "<?= 0?>/>
        </div>
    </div>
</div>
</body>
<?php
foreach ($bookList as $val) {

    ?>
    <form id="form_search" action="index.php" method="post">
        <div class="col-sm-12 form-group"><b><?= $val['title'] ?></b></div>
        <br>

        <div class="row">
            <div class="col-sm-6 form-group">€ <?= $val['price'] ?></div>

            <div class="col-sm-6">
                <input type="text"
                       name="search"
                       class="form-control"
                       value="<?= $val['id'] ?>"
                       hidden
                       required
                />
            </div>

        </div>
        <div class="row">
            <div class="col-sm-8 form-group"></div>

            <div class="col-sm-2 form-group">

                <?php
                if($val['stock']==0){
                    echo "Nicht auf Lager!";
                }else{

                ?>

                Menge:
                <select name="Anzahl">
                    <?php
                    $counter = $val['stock'];

                    for ($c = 1; $c <= $counter; $c++) {
                        echo "<option value=' .$c. '>" . $c . "</option>";
                    }
                    }
                    ?>
                </select>
            </div>
            <div class="col-sm-2 form-group">
                <input type="submit"
                       name="submit"
                       class="btn btn-primary btn-block"
                       value="Hinzufügen"/>
            </div>
            <div class="col-sm-12 form-group">
                <b><hr>
                </b></div>
        </div>
    </form>


    <?php
}
?>


</html>
