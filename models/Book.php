<?php


class Book
{
    private $id = null;
    private $title = '';
    private $price = 0;
    private $stock = 0;

    /**
     * Book constructor.
     * @param null $id
     * @param string $title
     * @param int $price
     * @param int $stock
     */
    public function __construct($id, $title, $price, $stock)
    {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->stock = $stock;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }




    public static function getAll(){
        //$files = json_decode("./PHP-23 bookdata.json", true);
        $files = json_decode('[{"id":1,"title":"Y-Solowarm","price":"6.25","stock":8},
{"id":2,"title":"Gembucket","price":"7.80","stock":3},
{"id":3,"title":"Bamity","price":"31.09","stock":7},
{"id":4,"title":"Bamity","price":"27.15","stock":4},
{"id":5,"title":"Bytecard","price":"11.81","stock":4},
{"id":6,"title":"Zoolab","price":"5.83","stock":7},
{"id":7,"title":"Keylex","price":"30.35","stock":7},
{"id":8,"title":"Mat Lam Tam","price":"24.44","stock":1},
{"id":9,"title":"Holdlamis","price":"10.31","stock":0},
{"id":10,"title":"Viva","price":"15.78","stock":7},
{"id":11,"title":"Lotstring","price":"14.66","stock":2},
{"id":12,"title":"Holdlamis","price":"16.79","stock":6},
{"id":13,"title":"Mat Lam Tam","price":"38.28","stock":4},
{"id":14,"title":"Sonair","price":"9.48","stock":10},
{"id":15,"title":"Duobam","price":"29.27","stock":7},
{"id":16,"title":"Overhold","price":"24.83","stock":6},
{"id":17,"title":"Hatity","price":"5.41","stock":1},
{"id":18,"title":"Latlux","price":"37.97","stock":8},
{"id":19,"title":"Cardguard","price":"26.13","stock":6},
{"id":20,"title":"Flexidy","price":"15.27","stock":1}]');

        $books = [];
        foreach ($files as $f){
            $books[] = new Book($f->id, $f->title,$f->price,$f->stock);
        }

        //$books = json_decode($books, true);

        return $books;
    }

    public static function getById($id){
        foreach (Book::getAll() as $book){
            if($book->id == $id){
                return $book;
            }
        }
        return null;
    }

    public static function getCount(){
    }

}